| Who | What | Keywords | Category| isReproducible |
| ------ | ------ |------ | ------ | ------ |
| Wolfgang Ludwig | WL1. Meld’s „Merge All“ menu toggles between the merged and unmerged state. The user is unable to know which state is active. | MergeAll_Toggle | UI_UX | Yes |
||WL2.	Meld sometimes does not redraw (thus, is not usable) when used via X11-forwarding over SSH.| X11_SSH | UI_UX<br/>Core / Performance | Not always |
||WL3.	Meld takes a very long time for diffing files with more than 25K lines of code. Probably, the reason might be the syntax highlighting. It looks like program is not responding (no user feedback). However, if you wait, you get a correct output.| Processing_Speed | Core / Performance| Yes |
| Thomas Hanson | TH1. Using meld on linux has been less than impressive for doing basic diffs mainly due to performance issues.  My opinion is meld is unusable for even basic compares ( inconsistent ).   I tried to use it yesterday and it never finished the diff of a file after many minutes. | Processing_Speed | Core / Performance | Yes |
| Celal Sahin | CS1. MELD freezes (seldomly) when ran from x11 session.  | X11_SSH | Core / Performance | Not always |
|| CS2. all complaints I received was more or less result of “merge all” button behaving “on/off”  | MergeAll_Toggle | UI_UX | Yes |
|| CS3. meld does not complain when you leave any “need merge” fields untouched | Need_Merge | UI_UX | Yes |
|| CS4. MELD has strange behavior on some buttons (see my earlier e-mail) | Buttons_behavior | UI_UX| ???
|| CS5. 3-way merge is better with 4 windows. MELD’s 3-way 3 window merge is confusing.| 3_Way_4_Windows | UI_UX| 
|| CS6. “merge all” button should not behave like a “on/off” button. | MergeAll_Toggle | UI_UX| Yes |
|| CS7. merge tool should complain when I exit if I left any unmerged hunks. | Need_Merge | UI_UX| Yes |
|| CS8. I also don’t like the looks & buttons of Meld | Looks | UI_UX | ??? |
| Manfred Eberler | ME1. The "Merge All" button behavior is confusing and causes problems. | MergeAll_Toggle | UI_UX | Yes |
|| ME2. The X11 forwarding over SSH is not functioning properly, sometimes. | X11_SSH | Core / Performance | Not always |
|| ME3. Processing big files takes ~3-4 minutes, seems to be not responding. | Processing_Speed | Core / Performance | Yes |
|| ME4. The "need merge" behavior should prompt/request approval before moving to next step.| Need_Merge | UI_UX | Yes |
|| ME5. 3-Way compare is better with 4-Windows. | 3_Way_4_Windows | UI_UX |
|| ME6. XML merges look confusing, but are correct at the end. | Looks | UI_UX | Yes |
|| ME7. Setting "--auto-merge" is a must. | FIXED | Configuration |
|| ME8. "I JUST WANT IT TO WORK" | Feature | Core / Performance |
|| ME9. Merging files bigger than ~1-2k lines is slow. | Processing_Speed | Core / Performance | Yes |
